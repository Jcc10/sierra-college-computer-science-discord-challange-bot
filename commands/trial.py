from commands.utility import parseInto

trialCommands = {}
trialAlais = {}
trialHelp = {}

# Handles stuff that the users handle.

# VIEW START
def view(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    if (parsedCommandLength > 2):
        s = " "
        challangeName = s.join(parsedCommand[2:])
        cText, cNote, cAuthor, cLanguage = database.getChallangeText(challangeName)
        if cText is None:
            msg = "Challange " + challangeName + " was not found.\n"
            msg += "Please check your spelling and try again.\n \n"
            msg += "HINT: Challange Names are Case-Sensitive."
        else:
            msg = "Challange: " + challangeName + "\n"
            msg += cNote
            msg += "Here is the `" + cLanguage + "` code:\n"
            msg += cText
            msg += "\nWhat's does it print?"
    else:
        msg = "You need to tell me which challange you want to view."
    return msg
trialCommands["VIEW"] = view
trialCommands["SEE"] = "VIEW"
trialHelp["VIEW"] = '''Display's the challange code block.
USAGE: `VIEW {CHALLANGE NAME}`
'''
# VIEW END


# CHECK START
def check(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    if (parsedCommandLength > 2):
        s = " "
        challangeName = s.join(parsedCommand[2:])
        cAnswer = database.getChallangeAnswer(challangeName)
        if cAnswer is None:
            msg = "Challange " + challangeName + " was not found.\n"
            msg += "Please check your spelling and try again.\n \n"
            msg += "HINT: Challange Names are Case-Sensitive."
        else:
            s = '\n'
            userAnswer = '`' + s.join(parseLines[1:]) + '\n`'
            if (userAnswer == cAnswer):
                msg = "Correct! Whoo!"
            else:
                msg = "Nope!\n \n"
                msg += "Hint: A answer will fail if you don't have the correct capatalisation."
    else:
        msg = "You need to tell me which challange to check."
    return msg
trialCommands["CHECK"] = check
trialAlais["VERIFY"] = "CHECK"
trialHelp["CHECK"] = '''Checks your answer aganst the recorded correct answer.
# WARNING: This is line and capatalisation sensitive.
USAGE: `CHECK {CHALLANGE}
{SINGLE OR
MULTILINE ANSWER}`
'''
# CHECK END


# NEW START
def new(message, start, database, commandHelp, client):
    challangeList, page = database.getChallangeNew()
    msg = "There are currently " + str(page+1) + " pages of challanges.\n"
    msg += "`LANGUAGE \t - CHALLANGE NAME\n"
    for row in challangeList:
        language = row[0]
        language = (language[:11] + '...') if len(language) > 13 else language + "\t"
        name = row[1]
        msg += language + " - " + name + "\n"
    msg += "`"
    return msg
trialCommands["NEW"] = new
trialHelp["NEW"] = '''Lists the 5 newest challanges.
'''
# NEW END

# LIST START
def list():
    return "Not Implemented."
trialCommands["LIST"] = new
trialHelp["LIST"] = '''Lists challanges
USAGE: `LIST` - Will print the last page of stuff.
USAGE: `LIST {PAGE}` - Prints the specified page.
'''
# LIST END
