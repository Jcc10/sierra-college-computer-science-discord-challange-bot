from dbConnection import database
from commands.utility import utilityCommands, utilityAlais, utilityHelp, parseInto, default
from commands.admin import adminCommands, adminAlais, adminHelp
from commands.trial import trialCommands, trialAlais, trialHelp
from commands.creator import creatorCommands, creatorAlais, creatorHelp
from commands.modder import modderCommands, modderAlais, modderHelp

commands = {**utilityCommands, **adminCommands, **trialCommands, **creatorCommands, **modderCommands}
commandAlais = {}
commandHelp = {**utilityHelp, **adminHelp, **trialHelp, **creatorHelp, **modderHelp}



def prosessCommand(command, message, start, client):
    # Get the function from switcher dictionary
    command = command.upper()
    if(message.channel.is_private == False):
        return "To keep the chalanges a challange, please talk to me here, in private chat!"
    else:
        alais = commandAlais.get(command, None)
        if alais is not None:
            command = alais
        func = commands.get(command, default)
        # Execute the function
        return func(message, start, database, commandHelp, client)
