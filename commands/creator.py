from commands.utility import parseInto

creatorCommands = {}
creatorAlais = {}
creatorHelp = {}



# ADD START
def addChalange(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    senderID = message.author.id
    access = database.checkAccess(message.author.id)
    if (access < 1):
        msg = "You do not have the required access to add a challange."
        return msg
    if (parsedLinesCount > 4 and parsedCommandLength >= 4):
        language = parsedCommand[2].upper()
        s = " "
        name = s.join(parsedCommand[3:])
        challange = {"note": "", "challange":"`", "test":"`"}
        challangeSet = False
        testSet = False
        adding = "note"
        msg = "Name: " + name + "\n"
        msg += "Language: " + language + "\n"
        for line in parseLines:
            lineStart = line[:3].upper()
            if(lineStart == start[:3]):
                continue
            elif(lineStart == "[x|"):
                if(line == "[x|CHALLANGE|x]"):
                    adding = "challange"
                    challangeSet = True
                    continue
                elif(line == "[x|TEST|x]"):
                    adding = "test"
                    testSet = True
                    continue
            else:
                challange[adding] += line + "\n"
        challange["challange"] += "`"
        challange["test"] += "`"
        if (challangeSet == False):
            msg = "challange not set"
            return msg
        if (testSet == False):
            msg = "test not set"
            return msg
        msg += "Note:\n" + challange["note"]
        msg += "Challange:\n" + challange["challange"] + "\n"
        msg += "Test:\n" + challange["test"]
        added = database.addChalange(name, senderID, challange["note"], challange["challange"],  challange["test"], language)
        if (added == False):
            msg = "Error adding challange, could there already be one with that name?"
    else:
        msg = "See `HELP ADD` for minimum valid command."
    return msg
creatorCommands["ADD"] = addChalange
creatorHelp["ADD"] = '''Adds a challange.
## REQUIRES ACCESS LEVEL 1 OR ABOVE ##
ADD {Language} {NAME}
{SINGLE OR
MULIT LINE
NOTE.}
[x|CHALLANGE|x]
{SINGLE OR
MULTI LINE
CHALLANGE TEXT}
[x|TEST|x]
{SINGLE OR
MULTI LINE
CASE SENSITIVE
ANSWER}
'''
# ADD END
