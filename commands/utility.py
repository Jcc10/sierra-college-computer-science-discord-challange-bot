utilityCommands = {}
utilityAlais = {}
utilityHelp = {}

#Utility functions

def parseInto(message):
    parseLines = message.content.split("\n")
    parsedCommand = parseLines[0].split(" ")
    parsedCommandLength = len(parsedCommand)
    parsedLinesCount = len(parseLines)
    return parseLines, parsedCommand, parsedCommandLength, parsedLinesCount

# Utility commands
# Stuff like help and repo, also the default command and help.


# DEFAULT START
def default(message, start, database, commandHelp, client):
    msg = "Invalid command.\n Enter `HELP` to see a list of commands."
    return msg
defaultHelp = '''I don't have help text for that command, are you sure it exsists?
Command `help` for a list of commands.
'''
# DEFAULT END


# HELLO START
def hello(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    msg = "Howdy! To see commands, say 'help'!"
    return msg
utilityCommands["HELLO"] = hello
utilityHelp["HELLO"] = '''A basic hello command.
This does not do anything special, it just ensures that the bot is online.
'''
# HELLO END

# REPO START
def repo(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    msg = "My source code is hosted on GitLab!\n"
    msg += "https://gitlab.com/Jcc10/sierra-college-computer-science-discord-challange-bot"
    return msg
utilityCommands["REPO"] = repo
utilityAlais["SOURCE"] = "REPO"
utilityHelp["REPO"] = '''Provides a link to the GitLab repo hosting a copy of my source code.'''
# REPO END


# HELP START
def help(message, start, database, commandHelp, client):
    senderID = message.author.id
    access = database.checkAccess(message.author.id)
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    if(parsedCommandLength >= 3):
        command = parsedCommand[2].upper()
        msg = commandHelp.get(command, defaultHelp)
    else:
        msg = '''Command List:
* HELLO:            Generic Hello Message.
* REPO:             Get a link to the GitLab Repo with my source-code.
* HELP:             This text.
* HELP HELP:        Help text for a specified command.
* NEW:              List of new challanges.
* LIST:             List of challanges. (latest page) (Not added yet)
* LIST {page}:      List of challanges. (specified page) (Not added yet)
* VIEW {challange}: View challange.
* CHECK {challange}:Check a answer.'''
        if (access > 0):
            msg += "\nYour access level is: `" + str(access) + "`\n"
        if (access >= 1):
            msg += '''    Level 1:
* ADD:              Add a challange. (View help docs for more)
* MODIFY:           Modify Your Own challange. (Not added yet)
* RENAME:           Renames a challange. (Not added yet)
* FULLVIEW:         View both the challange and the answer of your own challange. (Not added yet)
'''
        if (access >= 2):
            msg += '''    Level 2:
* SUPERMODIFY:      Modify Anyone's challange. (Not added yet)
* SUPERFULLVIEW:    View both the challange and the answer of any challange. (Not added yet)
'''
        if (access >= 3):
            msg += '''    Level 3:
* SUPERAUTHOR:      Change the author ID for a challange. (Not added yet)
* AUTHORISE:        Authorise a user. (Less then your level.)
'''
        if (access >= 3):
            msg += '''    Level 4
* REBOOT:           Reboots the bot. (Not added yet)
* SHUTDOWN:         Shutdown the bot. (Not added yet)
'''
    return msg
utilityCommands["HELP"] = help
utilityHelp["HELP"] = '''The help command:
USAGE:You can enter a command after it for detailed help.
`HELP {COMMAND}`
EG: `HELP LIST` provides the help text for the `LIST` command.
'''
# HELP END
