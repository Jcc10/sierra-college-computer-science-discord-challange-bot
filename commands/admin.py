from commands.utility import parseInto

# ADMIN COMMANDS
# Only commands related to user access levels

adminCommands = {}
adminAlais = {}
adminHelp = {}

# ACCESS START
def access(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    senderID = message.author.id
    access = database.checkAccess(message.author.id)
    msg = "Your ID is `" + str(senderID) + "` and your access level is `" + str(access) + "`."
    if (parsedCommandLength >= 3):
        accessOther = database.checkAccess(parsedCommand[2], False)
        if (accessOther == -1):
            msg += "\n I have yet to record him in my user database."
        elif (access > accessOther):
            msg += "\n `" + parsedCommand[2] + "`'s access level is " + str(accessOther)
        else:
            msg += "\n `" + parsedCommand[2] + "`'s access level above yours."
    return msg
adminCommands["ACCESS"] = access
adminAlais["ME"] = "ACCESS"
adminAlais["AUTH"] = "ACCESS"
adminHelp["ACCESS"] = '''A command that shows your ID and access level. (And someone elses if ID is provided)
USAGE: `ACCESS` for just your ID and level
USAGE: `ACCESS {ID}` for your ID and level, and the other person's ID and level (if below your level)
ID's are used since usernames can change and I don't feel like dealing with tracking that.
'''
# ACCESS END


# AUTHORIZE START
def authorize(message, start, database, commandHelp, client):
    parseLines, parsedCommand, parsedCommandLength, parsedLinesCount = parseInto(message)
    senderID = message.author.id
    access = database.checkAccess(message.author.id)
    if (access < 3):
        msg = "You do not have the required access to add a challange."
        return msg
    if (parsedCommandLength >= 4):
        user = parsedCommand[2]
        level = int(parsedCommand[3])
        if (level < access):
            accessOther = database.checkAccess(parsedCommand[2], False)
            if (accessOther < access or str(senderID) == user):
                database.setAccess(user, level)
                msg = "User: '" + str(user) + "' set to access level '" + str(level) + "'"
            else:
                msg = "You don't have a higher level then the user you are trying to set."
        else:
            msg = "You need a higher level then the level you give to others."
    else:
        msg = "You need to tell me what user and access level to set them to."
    return msg
adminCommands["AUTHORIZE"] = authorize
adminHelp["AUTHORIZE"] = '''Sets authority level of a user.
Both the users current and future level must be below your level.
## REQUIRES ACCESS LEVEL 3 OR ABOVE ##
USAGE: `AUTHORIZE` {USER ID} {ACCESS LEVEL NUMBER}
'''
# AUTHORIZE END
