import sqlite3
import math

class challangeDatabase:
    def __init__(self):
        self.conn = sqlite3.connect('challange-db.sqlite')

    def getChallangeText(self, challange):
        c = self.conn.cursor()
        t = (challange,)
        c.execute('SELECT `ChallangeText`, `ChallangeNote`, `Author`, `Language` FROM `challanges` WHERE `ChallangeName`=?', t)
        r = c.fetchone()
        if r is None:
            return None, None, None, None
        else:
            return r[0], r[1], r[2], r[3]
        return
    def getChallangeAnswer(self, challange):
        c = self.conn.cursor()
        t = (challange,)
        c.execute('SELECT `ChallangeTest` FROM `challanges` WHERE `ChallangeName`=?', t)
        r = c.fetchone()
        if r is None:
            return None
        else:
            return r[0]
    def addUser(self, id):
        c = self.conn.cursor()
        t = [(id,)]
        c.executemany('INSERT INTO `Users`(`UserID`) VALUES (?);', t)
        self.conn.commit()

    def checkAccess(self, id, add=True):
        c = self.conn.cursor()
        t = (id,)
        c.execute('SELECT `L1`, `L2`, `L3`, `L4` FROM `Users` WHERE `UserID`=?', t)
        r = c.fetchone()
        if r is None:
            if add:
                self.addUser(id)
                return 0
            else:
                return -1
        else:
            if (r[3] == 1):
                return 4
            elif (r[2] == 1):
                return 3
            elif (r[1] == 1):
                return 2
            elif (r[0] == 1):
                return 1
            else:
                return 0
    def setAccess(self, id, level):
        c = self.conn.cursor()
        if (level == 4):
            s = (1, 1, 1, 1, id)
        elif (level == 3):
            s = (1, 1, 1, 0, id)
        elif (level == 2):
            s = (1, 1, 0, 0, id)
        elif (level == 1):
            s = (1, 0, 0, 0, id)
        else:
            s = (0, 0, 0, 0, id)
        t = [s]
        c.executemany('UPDATE `Users` SET `L1` = ?, `L2` = ? , `L3` = ?, `L4` = ? WHERE `UserID` = ?', t)
        self.conn.commit()
    def addChalange(self, name, author, note, text, test, language):
        c = self.conn.cursor()
        t = (name,)
        c.execute('SELECT `ChallangeID` FROM `challanges` WHERE `ChallangeName`=?', t)
        r = c.fetchone()
        if r is None:
            s = (language, author, name, note, text, test)
            t = [s]
            c.executemany('INSERT INTO `challanges`(`ChallangeID`,`Language`,`Author`,`ChallangeName`,`ChallangeNote`,`ChallangeText`,`ChallangeTest`) VALUES (NULL,?,?,?,?,?,?);', t)
            self.conn.commit()
            return True
        else:
            return False
    def challangeCount(self):
        c = self.conn.cursor()
        c.execute('SELECT count(`ChallangeID`) FROM `challanges`')
        r = c.fetchone()
        return math.ceil(r[0]/5)
    def getChallangeNew(self, page=None):
        c = self.conn.cursor()
        c.execute('SELECT `Language`,`ChallangeName` FROM `challanges`  ORDER BY `ChallangeID` DESC LIMIT 0, 5;')
        r = c.fetchall()
        return r, page
    def getChallangePage(self, page=None):
        c = self.conn.cursor()
        if page is None:
            page = self.challangeCount()
        page -= 1
        offset = page * 5
        t = (offset,)
        c.execute('SELECT `Language`,`ChallangeName` FROM `challanges`  ORDER BY `ChallangeID` ASC LIMIT ?, 5;', t)
        r = c.fetchall()
        return r, page

database = challangeDatabase()
