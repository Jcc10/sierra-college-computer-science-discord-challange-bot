# Work with Python 3.6
import discord
from realConfig import TOKEN, START
from commands.commands import prosessCommand

client = discord.Client()

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    # all commands must start with a '!CB' so if it doesn't, we can ignore.
    parseLines = message.content.split("\n")
    parsedCommand = parseLines[0].split()
    parsedCommandLength = len(parsedCommand)
    parsedLinesCount = len(parseLines)
    msg = "Generic Error."
    if(parsedCommandLength >= 1):
        if not(parsedCommand[0].upper() == START):
            return
    else:
        return

    if not(parsedCommandLength >= 2):
        msg = "You need to actually input a command."
    else:
        msg = prosessCommand(parsedCommand[1], message, START, client)
    # send the message out.
    await client.send_message(message.author, msg)

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run(TOKEN)
