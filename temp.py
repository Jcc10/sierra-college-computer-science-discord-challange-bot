
        if (parsedCommand[1] == 'hello'):
            if (message.channel.is_private == False):
                msg = "To keep the chalanges a challange, please talk to me in private chat!"
        elif (parsedCommand[1] == 'addChallange'):
            if (parsedCommandLength > 2):
                name = parsedCommand[2]
                challange = {"challange":"`", "test":"`"}
                adding = ""
                parseLines = message.content.split("\n")
                msg = "Name: " + name + "\n"
                for line in parseLines:
                    start = line[:3]
                    if(start == "!CB"):
                        continue
                    elif(start == "[x|"):
                        if(line == "[x|CHALLANGE|x]"):
                            adding = "challange"
                            continue
                        elif(line == "[x|TEST|x]"):
                            adding = "test"
                            continue
                    else:
                        challange[adding] += line + "\n"
                challange["challange"] += "`"
                challange["test"] += "`"
                msg += "Challange:\n" + challange["challange"] + "\n"
                msg += "Test:\n" + challange["test"]
                challanges = [(name, challange["challange"], challange["test"])]
                c.executemany('INSERT INTO `challanges`(`ChallangeID`,`ChallangeName`,`ChallangeText`,`ChallangeTest`) VALUES (null,?,?,?);', challanges)
                conn.commit()
        elif parsedCommand[1] == 'see':
            if(parsedCommandLength > 2):
                t = (parsedCommand[2],)
                c.execute('SELECT `ChallangeName`,`ChallangeText` FROM `challanges` WHERE `ChallangeName`=?', t)
                r = c.fetchone()
                msg = "Challange: " + r[0] + "\n"
                msg += "Here is the code:\n"
                msg += r[1]
                msg += "\nWhat's does it print?"
        elif parsedCommand[1] == 'check':
            if(parsedCommandLength > 2):
                t = (parsedCommand[2],)
                c.execute('SELECT `ChallangeTest` FROM `challanges` WHERE `ChallangeName`=?', t)
                r = c.fetchone()
                text = "`"
                parseLines = message.content.split("\n")
                for line in parseLines:
                    start = line[:3]
                    if(start == "!CB"):
                        continue
                    text += line + "\n"
                text += "`"
                if (text == r[0]):
                    msg = "You are correct!"
                else:
                    msg = "Wrong!"
        elif parsedCommand[1] == 'killAndDie':
            if (parsedCommandLength > 2):
                if (parsedCommand[2] == 'TestSystem'):
                    print("Shutting Down")
                    msg = 'Bot shutting down.'
                    await client.send_message(message.channel, msg)
                    exit(0)
                else:
                    msg = 'Invalid Shutdown Key.'
            else:
                msg = 'Shutdown Key Required.'
